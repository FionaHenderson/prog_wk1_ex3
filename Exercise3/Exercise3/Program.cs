﻿using System;

namespace Exercise3
{
    class MainClass
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello and Welcome, what is your name?");
            var name = Console.ReadLine();

            Console.WriteLine($"Pleasure to meet you {name}, Thank you for introducing yourself.");
        }
    }
}
